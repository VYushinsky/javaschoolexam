package com.tsystems.javaschool.tasks.calculator;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */

    public static String evaluate(String statement) {
        ArrayList<Double> numbers = new ArrayList<>();
        ArrayList<Character> sign = new ArrayList<>();

        try {
            for (int i = 0; i < statement.length(); i++) {
                char c = statement.charAt(i);
                if (space(c)) {
                    continue;
                }
                if (c == '(') {
                    sign.add('(');
                }
                else if (c == ')') {
                    while (sign.get(sign.size() - 1) != '(')
                        calculations(numbers, sign.remove(sign.size() - 1));
                    sign.remove(sign.size() - 1);
                } else if (operator(c)) {
                    while (!sign.isEmpty() && priority(sign.get(sign.size() - 1)) >= priority(c))
                        calculations(numbers, sign.remove(sign.size() - 1));
                    sign.add(c);
                } else {
                    StringBuilder stringBuilder = new StringBuilder();
                    while (i < statement.length() && !space(statement.charAt(i)) && !operator(statement.charAt(i)) && !bracket(statement.charAt(i))) {
                        stringBuilder.append(statement.charAt(i));
                        i++;
                    } i--;
                    numbers.add(Double.parseDouble(stringBuilder.toString()));
                }
            }
            while (!sign.isEmpty()) {
                calculations(numbers, sign.remove(sign.size() - 1));
            }
            DecimalFormat decimalFormat = new DecimalFormat("###.####");
            decimalFormat.setRoundingMode(RoundingMode.CEILING);
            return (numbers.get(0) != null && !numbers.get(0).isInfinite() && !numbers.get(0).isNaN()) ? decimalFormat.format(numbers.get(0)) : null;
        } catch (Exception exception) {
            return null;
        }
    }

    static void calculations(ArrayList<Double> numbers, char operator) {
        double num2 = numbers.remove(numbers.size() - 1);
        double num1 = numbers.remove(numbers.size() - 1);
        switch (operator) {
            case '+':
                numbers.add(num1 + num2);
                break;
            case '-':
                numbers.add(num1 - num2);
                break;
            case '*':
                numbers.add(num1 * num2);
                break;
            case '/':
                numbers.add(num1 / num2);
                break;
        }
    }

    static int priority(char c) {
        switch (c) {
            case '+':
            case '-':
                return 1;
            case '*':
            case '/':
                return 2;
            default:
                return -1;
        }
    }

    static boolean space(char c) { return c == ' ';}

    static boolean bracket(char c) { return c == '(' || c == ')'; }

    static boolean operator(char c) { return c == '+' || c == '-' || c == '*' || c == '/'; }
}


