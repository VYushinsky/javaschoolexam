package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if (inputNumbers.contains(null) || inputNumbers.size() >= Math.pow(2, 31) - 2) throw new CannotBuildPyramidException();

        Collections.sort(inputNumbers);
        int size = inputNumbers.size();
        int pyrHeigth = 0;
        while (size != 0) {
            size -= pyrHeigth + 1 ;
            if (size < 0) {
                throw new CannotBuildPyramidException();
            } else pyrHeigth++;
        }

        int[][] back = new int[pyrHeigth][pyrHeigth * 2 - 1];
        int count = 0;
        for (int i = 0; i < pyrHeigth; i++) {
            for (int j = pyrHeigth - i - 1; j < pyrHeigth + i + 1; j += 2){
                back[i][j] = inputNumbers.get(count);
                count++;
            }
        }
        return back;
    }
}
